extends CharacterBody3D

class_name Battler

enum ETeam {
	PLAYER, ENEMY, NEUTRAL
}

var team : ETeam = ETeam.PLAYER

@onready var skill_executor := $SkillExecutor as SkillExecutor

func is_moving():
	return false
