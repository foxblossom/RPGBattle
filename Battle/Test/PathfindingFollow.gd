extends CharacterBody3D

@onready var agent  := $NavigationAgent3D as NavigationAgent3D

@export var speed: float

func _ready():
	agent.target_position = position

func _physics_process(_delta: float) -> void:
	var direction = to_local(agent.get_next_path_position()).normalized()

	velocity = direction * speed * _delta
	move_and_slide()


func _on_timer_timeout():
	agent.target_position = $/root/Node3D/Player.position
