extends Node

class_name EnemyBehaviour

var action_index = 0

var waiting    : bool  = false
var wait_timer : float = 0

@onready var skill_executor = $'../SkillExecutor' as SkillExecutor

# Increment action index and figure out what action an AI will take next
func get_next_action():
	action_index += 1
	next_action()
	
# Override this function for the actual logic of what the enemy will do next.
func next_action():
	pass

# The enemy will wait for x seconds before trying to get another action, during which time it will auto attack.
func wait(seconds):
	waiting = true
	wait_timer = seconds

func _process(delta):
	if waiting:
		wait_timer -= delta
		if wait_timer <= 0:
			waiting = false
			get_next_action()
			

func execute_skill(skill :Skill):
	skill_executor.cast(skill)
