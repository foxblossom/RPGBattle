extends Battler

class_name Enemy

@onready var behaviour := $Behaviour as EnemyBehaviour

func _ready():
	team = ETeam.ENEMY
	
	skill_executor.connect("FinishedSkill", on_previous_skill_complete)
	behaviour.get_next_action()
	
func on_previous_skill_complete():
	behaviour.get_next_action()
