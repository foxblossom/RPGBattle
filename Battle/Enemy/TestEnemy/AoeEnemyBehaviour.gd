extends EnemyBehaviour

@onready var circle_aoe := preload("res://Battle/Skill/Test/TestCircleAOE.tres") as Skill
@onready var line_aoe := preload("res://Battle/Skill/Test/TestLineAOE.tres") as Skill


func next_action():
	
	if action_index % 4 == 1:
		return execute_skill(line_aoe)
	if action_index % 4 == 3:
		return execute_skill(circle_aoe)
		
	return wait(5)
