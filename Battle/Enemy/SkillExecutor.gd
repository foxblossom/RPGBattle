extends Timer

class_name SkillExecutor

signal FinishedCast
signal FinishedSkill

var active_skill          : Skill
var active_cast_behaviour : CastBehaviour

@onready var caster  = $'..' as Battler
@onready var castbar = $'../CastBar' as CastBar

func _ready():
	connect("timeout", cast_finished)

func is_casting() -> bool:
	return active_cast_behaviour != null
	
func is_active() -> bool:
	return active_skill != null

func cast(skill: Skill):
	active_skill = skill
	
	# Start cast timer. Cleanup & actual skill stuff happens after this expires
	start(skill.cast_time)
	
	active_cast_behaviour = skill.cast_behaviour.instantiate()
	$/root.add_child(active_cast_behaviour)	

	castbar.initialize(skill.skill_name)

	if active_cast_behaviour is CastBehaviour:
		active_cast_behaviour.skill = active_skill
		active_cast_behaviour.caster = caster
		active_cast_behaviour.run()
		

func _process(delta):
	if is_casting():
		var percentage = 100 - ((time_left / wait_time) * 100)
		castbar.update(percentage)
		
# Cast is finished - cleanup the cast object and create the action object.
func cast_finished():
	emit_signal("FinishedCast")
	
	castbar.remove()

	if active_cast_behaviour is CastBehaviour:
		active_cast_behaviour.destroy()
	else:
		active_cast_behaviour.queue_free()
		active_cast_behaviour = null

	var action_object = active_skill.action_behaviour.instantiate()
	$/root.add_child(action_object)
	
	if action_object is ActionBehaviour:
		action_object.skill = active_skill
		action_object.caster = caster
		action_object.connect("Finished", skill_finished)
		action_object.run()

# Skill is totally complete
func skill_finished():
	active_skill = null
	emit_signal("FinishedSkill")
