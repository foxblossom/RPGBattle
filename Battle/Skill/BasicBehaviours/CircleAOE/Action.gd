extends ActionBehaviour

# Called when the node enters the scene tree for the first time.
func run():
	global_position = caster.global_position
	scale = (Vector3.ONE * skill.aoe_scale)

func _on_timer_timeout():
	finish()

func _on_area_3d_body_entered(body):
	if body is Battler and should_hit(body):
		print(body.name + " Hit by " + skill.skill_name)
