extends Node3D

class_name ActionBehaviour

var caster : Battler
var target : Node3D

var skill : Skill

signal Finished

func run():
	pass
	
func finish():
	destroy()
	print("Destroy action")
	emit_signal("Finished")
	
func destroy():
	queue_free()

func should_hit(battler : Battler) -> bool:
	if skill.target_type == Skill.ESkillTarget.All:
		return true
		
	if battler.team == caster.team and skill.target_type == Skill.ESkillTarget.Allies:
		return true
		
	if battler.team != caster.team and skill.target_type == Skill.ESkillTarget.Enemies:
		return true
	
	return false
