extends Node3D

class_name CastBar

@onready var bar          = $Sprite3D/SubViewport/Control/ProgressBar as ProgressBar
@onready var label        = $Sprite3D/SubViewport/Control/Label as Label
@onready var base_control = $Sprite3D/SubViewport/Control as Control

func initialize(skilL_name :String):
	label.text = skilL_name
	bar.value = 0
	base_control.modulate = Color.WHITE
	
func update(percentage):
	bar.value = percentage
	
func remove():
	base_control.modulate = Color.TRANSPARENT
