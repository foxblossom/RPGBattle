extends Resource

class_name Skill

enum ESkillTarget {
	Allies,
	Enemies,
	All
}

@export var skill_name  : String       = "Skill"
@export var mp_cost     : int          = 0
@export var cast_time   : float        = 1.0
@export var aoe_scale   : float        = 1.0
@export var target_type : ESkillTarget = ESkillTarget.Enemies

@export var cast_behaviour : PackedScene
@export var action_behaviour : PackedScene

