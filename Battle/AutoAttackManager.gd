extends Area3D

class_name AutoAttackManager

@onready var battler        = $'..' as Battler
@onready var skill_executor = $'../SkillExecutor' as SkillExecutor

var auto_attack_timer : float = 3

var targets : Array = []

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if auto_attack_timer > 0:
		auto_attack_timer -= delta
	
	if !skill_executor.is_active() and !battler.is_moving() and auto_attack_timer <= 0:
		auto_attack()

func auto_attack():
	if targets.size() > 0:
		var target = targets[0]
		auto_attack_timer = 2.0
		print(battler.name + " Auto Attacks " + target.name)

func _on_body_entered(body):
	if body is Battler and body.team != battler.team:
		targets.append(body)

func _on_body_exited(body):
	if body is Battler and body.team != battler.team:
		var index = targets.find(body)
		
		if index > -1:
			targets.remove_at(index)
