extends Battler

class_name PlayerBattler

@onready var camera   := $/root/Node3D/Camera3D as Camera3D
@onready var preview  := $PreviewAgent as NavigationAgent3D
@onready var agent    := $MovementAgent as NavigationAgent3D

@export var speed: float

var unit_selected   : bool = false
var has_mouse_focus : bool = false

func _ready():
	team = ETeam.PLAYER
	
	preview.target_position = position
	agent.target_position = position

func _physics_process(_delta: float) -> void:
	# Move the unit towards the desired location
	if !agent.is_target_reached():
		var direction = to_local(agent.get_next_path_position()).normalized()

		velocity = direction * speed * _delta
		move_and_slide()
	
	# Unit is currently selected - put the preview position under the mouse
	if unit_selected:
		preview.target_position = get_mouse_pos()
		# This is required in order to make the path show up
		preview.get_next_path_position()

func _input(event):
	if  event is InputEventMouseButton:
		# Unit is selected - upon clicking, the unit should pathfind to the correct location
		if unit_selected:
			agent.target_position = get_mouse_pos()
			preview.target_position = get_mouse_pos()
			deselect()
			
		# Unit is clicked and not selected
		elif has_mouse_focus:
			select()

func get_mouse_pos(): 
	var position2D = get_viewport().get_mouse_position()
	var dropPlane  = Plane(Vector3(0, 1, 0), 0)
	return dropPlane.intersects_ray(camera.project_ray_origin(position2D), camera.project_ray_normal(position2D))

func select():
	preview.debug_enabled = true
	unit_selected = true
	
func deselect():
	preview.debug_enabled = false
	unit_selected = false

func _on_mouse_entered():
	has_mouse_focus = true

func _on_mouse_exited():
	has_mouse_focus = false
